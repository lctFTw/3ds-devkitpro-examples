#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <3ds.h>
#include <sf2d.h>

extern const struct {
 unsigned int width;
 unsigned int height;
 unsigned int bytes_per_pixel;
 unsigned char pixel_data[];
} image;

int main()
{
	sf2d_init();
	
	sf2d_set_clear_color(RGBA8(0x40, 0x40, 0x40, 0xFF));
	
	sf2d_texture *tex_image = sf2d_create_texture_mem_RGBA8(image.pixel_data, image.width, image.height, TEXFMT_RGBA8, SF2D_PLACE_RAM);
	
	while (aptMainLoop())
	{
		gspWaitForVBlank();
		hidScanInput();


		u32 kDown = hidKeysDown();
		if (kDown & KEY_START)
		{
			break; //Break in order to return to hbmenu
		}

		sf2d_start_frame(GFX_TOP, GFX_LEFT);
		sf2d_draw_texture(tex_image, 0, 0);
		sf2d_end_frame();
		//Swap the buffers
		sf2d_swapbuffers();
		

	}

	
	sf2d_free_texture(tex_image);
	
	sf2d_fini();

	return 0;
}