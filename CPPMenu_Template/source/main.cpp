#include <string.h>
#include <3ds.h>
#include <stdio.h>


//Structs
struct Selected {
	int Sel = 1;
	int Max;
	int Min;
} mainMenu;


//Vars
int MenuID = 1; //MenuID 1 is mainMenu

//Functions
void drawMenu(bool topScreen);
void checkMenu();
void Setup();

void Setup()
{
	gfxInitDefault();
	gfxSet3D(false);
	consoleInit(GFX_TOP, NULL);

	mainMenu.Max = 4;
	mainMenu.Min = 1;
	drawMenu(true);
}

int main()
{
	Setup();

	// Main loop
	while (aptMainLoop())
	{
		gspWaitForVBlank();
		hidScanInput();


		u32 kDown = hidKeysDown();
		if (kDown & KEY_START)
		{
			break; //Break in order to return to hbmenu
		}
		if (kDown & KEY_B)
		{
			if (MenuID == !1) 
			{
				MenuID = 1;
				drawMenu(true);
			}
		}
		if (kDown & KEY_UP)
		{
			switch (MenuID)
			{
			case 1:
				if (mainMenu.Min == mainMenu.Sel) {} else 
				{
					mainMenu.Sel = mainMenu.Sel - 1;
					drawMenu(true);
				}
				break;
			}
		}
		if (kDown & KEY_DOWN)
		{
			switch (MenuID)
			{
			case 1:
				if (mainMenu.Max == mainMenu.Sel) {}
				else
				{
					mainMenu.Sel = mainMenu.Sel + 1;
					drawMenu(true);
				}
				break;
			}
		}
		if (kDown & KEY_A)
		{
			switch (MenuID)
			{
			case 1:
				checkMenu();
				break;
			}
		}

		// Flush and swap frame-buffers
		gfxFlushBuffers();
		gfxSwapBuffers();
	}

	gfxExit();
	return 0;
}

void drawMenu(bool topScreen)
{
	if (topScreen == true)
	{
		consoleInit(GFX_TOP, NULL);
	}
	else
	{
		consoleInit(GFX_BOTTOM, NULL);
	}

	consoleClear();
	switch (MenuID)
	{
	case 1:
		if (mainMenu.Sel == 1) { printf(">"); }
		printf("Option 1\n");
		if (mainMenu.Sel == 2) { printf(">"); }
		printf("Option 2\n");
		if (mainMenu.Sel == 3) { printf(">"); }
		printf("Option 3\n");
		if (mainMenu.Sel == 4) { printf(">"); }
		printf("Option 4\n");
		break;
	}
}

void checkMenu()
{
	switch (MenuID)
	{
	case 1:

		switch (mainMenu.Sel)
		{
		case 1:
			MenuID = 0;
			consoleClear();
			printf("You selected 'Option 1'! \nPress 'B' to go back.");
			break;
		case 2:
			MenuID = 0;
			consoleClear();
			printf("You selected 'Option 2'! \nPress 'B' to go back.");
			break;
		case 3:
			MenuID = 0;
			consoleClear();
			printf("You selected 'Option 3'! \nPress 'B' to go back.");
			break;
		case 4:
			MenuID = 0;
			consoleClear();
			printf("You selected 'Option 4'! \nPress 'B' to go back.");
			break;
		}

		break;
	}
}