#include <string.h>
#include <3ds.h>

int main()
{
	gfxInitDefault();
	gfxSet3D(false);
	consoleInit(GFX_TOP, NULL);
	//consoleInit(GFX_BOTTOM, NULL);
	
	//One time run code here

	// Main loop
	while (aptMainLoop())
	{
		gspWaitForVBlank();
		hidScanInput();


		u32 kDown = hidKeysDown();
		if (kDown & KEY_START)
		{
			break; //Break in order to return to hbmenu
		}

		// Flush and swap frame-buffers
		gfxFlushBuffers();
		gfxSwapBuffers();
	}

	gfxExit();
	return 0;
}