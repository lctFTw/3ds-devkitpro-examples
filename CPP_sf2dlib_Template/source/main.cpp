#include <string.h>
#include <3ds.h>
#include <stdio.h>
#include <stdlib.h>
#include <sf2d.h>

#define CONFIG_3D_SLIDERSTATE (*(float *)0x1FF81080)


int main()
{

	sf2d_init();
	//sf2d_set_clear_color(RGBA8(0x40, 0x40, 0x40, 0xFF));
	sf2d_set_3D(1);

	float offset3d = 0.0f;
	u16 touch_x = 320 / 2;
	u16 touch_y = 240 / 2;
	touchPosition touch;
	circlePosition circle;

	// Main loop
	while (aptMainLoop())
	{
		//gspWaitForVBlank();
		hidScanInput();
		hidCircleRead(&circle);


		u32 kDown = hidKeysDown();
		if (kDown & KEY_START)
		{
			break; //Break in order to return to hbmenu
		}
		if (kDown & KEY_TOUCH) 
		{
			hidTouchRead(&touch);
			touch_x = touch.px;
			touch_y = touch.py;
		}


		offset3d = CONFIG_3D_SLIDERSTATE * 30.0f;

		sf2d_start_frame(GFX_TOP, GFX_LEFT);
		sf2d_draw_rectangle(30, 100, 40, 60, RGBA8(0xFF, 0x00, 0xFF, 0xFF));
		sf2d_end_frame();

		sf2d_start_frame(GFX_TOP, GFX_RIGHT);
		sf2d_draw_rectangle(20, 60, 40, 40, RGBA8(0xFF, 0x00, 0x00, 0xFF));
		sf2d_end_frame();

		gfxFlushBuffers();
		gfxSwapBuffers();
	}

	gfxExit();
	return 0;
}